Display the dependency tree of a certain module based on your yarn.lock file

<h1>Usage</h1>
<pre>
	yarn-dependency-tree ./yarn.lock lodash
</pre>

It will display which of your other modules use lodash and at which versions. The tool came up needed in situations where I have duplicate versions of a certain module used in my project, and I cannot find which of my other dependencies cause this conflict.

