#!/usr/bin/env node

const readline = require('readline');
const loadGraph = require('./load-graph');

const readlineInterface = readline.createInterface({
	input: process.stdin,
	output: process.stdout,
	terminal: false,
});

if (process.argv.length < 3) {
	console.error('Give me Yarn.lock');
	process.exit();
}

const yarnLockFilePath = process.argv[2];

const graph = loadGraph(yarnLockFilePath);

const tab = level =>
	Array(level * 4)
		.fill(null)
		.map(() => ' ')
		.join('');

const colored = (text, level) => `${level === 0 ? '\x1b[41m' : '\x1b[36m'}${text}\x1b[0m`;

const printConsumerTree = (componentNodes, level = 0) =>
	componentNodes.forEach(componentNode => {
		const indent = tab(level);
		const componentName = colored(componentNode.name, level);
		const { resolvedVersion } = componentNode;
		console.log(`${indent}${componentName}@${resolvedVersion}`);
		if (componentNode.dependables) {
			componentNode.dependables.forEach(dependable => printConsumerTree([dependable], level + 1));
		}
	});

const analyze = componentName => {
	if (!(componentName in graph)) {
		console.error('Unknown component');
		return false;
	}

	const component = graph[componentName];
	printConsumerTree(component);
};

if (process.argv.length > 3) {
	const componentName = process.argv[3];
	analyze(componentName);
	process.exit();
}

console.log(new Date(), 'Enter component name: ');
readlineInterface.on('line', analyze);
