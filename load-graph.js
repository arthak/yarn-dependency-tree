const fs = require('fs');

const getFileContent = path => fs.readFileSync(path, { encoding: 'ascii' });

const parseTitleRow = line => {
	if (line.startsWith('#')) {
		return null;
	}
	if (line.length === 0) {
		return null;
	}
	const titleRow = line.slice(0, line.length - 1);
	const nodeRefs = titleRow
		.split(', ')
		.map(quotedNodeRef => quotedNodeRef.replace(/"/g, ''))
		.map(nodeRef => {
			const indexOfLastAt = nodeRef.lastIndexOf('@');
			if (indexOfLastAt < 0) {
				throw new Error(`Invalid Node reference: ${JSON.stringify(nodeRef)}`);
			}
			const name = nodeRef.substr(0, indexOfLastAt);
			const versionRange = nodeRef.substr(indexOfLastAt + 1);
			return {
				name,
				versionRange,
			};
		});
	if (nodeRefs.find((nodeRef, index) => index > 0 && nodeRef.name !== nodeRefs[0].name)) {
		throw new Error(`Invalid Node references: ${titleRow}`);
	}

	return {
		name: nodeRefs[0].name,
		requiredVersionRanges: nodeRefs.map(({ versionRange }) => versionRange),
	};
};

const parseResolvedVersion = versionRow => versionRow.replace(/^.*version "/g, '').replace(/"/g, '');

const parseDependencies = lines => {
	const indexOfDependenciesTitleRow = lines.findIndex(
		(line, index) => index > 1 && line.includes('dependencies:')
	);
	const indexOfOptionalDependenciesTitleRow = lines.findIndex(
		(line, index) => index > 1 && line.includes('optionalDependencies:')
	);
	const dependencies = {};
	if (indexOfDependenciesTitleRow >= 0) {
		lines
			.filter(
				(_, index) =>
					indexOfOptionalDependenciesTitleRow < 0 || index < indexOfOptionalDependenciesTitleRow
			)
			.filter((_, index) => index > indexOfDependenciesTitleRow)
			.map(dependencyRow => dependencyRow.trim())
			.forEach(dependencyRow => {
				const piecesOfDependency = dependencyRow.split(' ');
				const name = piecesOfDependency[0].replace(/"/g, '');
				const requiredVersionRange = piecesOfDependency[1].replace(/"/g, '');
				dependencies[name] = {
					requiredVersionRange,
				};
			});
	}
	return dependencies;
};

const parseYarnLock = yarnLock =>
	yarnLock
		.replace(/#.*\n/g, '')
		.trim()
		.split('\n\n')
		.map(nodeChunk => {
			const lines = nodeChunk.split('\n');
			const titleRowData = parseTitleRow(lines[0]);
			if (!titleRowData) {
				return null;
			}
			const resolvedVersion = parseResolvedVersion(lines[1]);
			const dependencies = parseDependencies(lines);
			return {
				name: titleRowData.name,
				requiredVersionRanges: titleRowData.requiredVersionRanges,
				resolvedVersion,
				dependencies,
			};
		})
		.filter(node => Boolean(node));

const linkDependencies = nodes =>
	nodes.forEach(node => {
		Object.keys(node.dependencies).forEach(dependencyName => {
			const requiredVersionRange = node.dependencies[dependencyName].requiredVersionRange;

			for (let i = 0; i < nodes.length; i++) {
				if (
					nodes[i].name === dependencyName &&
					nodes[i].requiredVersionRanges.includes(requiredVersionRange)
				) {
					node.dependencies[dependencyName].link = nodes[i];
					if (!('dependables' in nodes[i])) {
						nodes[i].dependables = [];
					}
					nodes[i].dependables.push(node);
				}
			}
		});
	});

const createAssembledGraph = nodes =>
	nodes.reduce(
		(graph, node) => ({
			...graph,
			[node.name]: [...(graph[node.name] || []), node],
		}),
		{}
	);

module.exports = yarnLockFilePath => {
	const yarnLock = getFileContent(yarnLockFilePath);
	const nodes = parseYarnLock(yarnLock);
	linkDependencies(nodes);
	return createAssembledGraph(nodes);
};
